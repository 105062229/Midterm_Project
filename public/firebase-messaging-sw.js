importScripts('https://www.gstatic.com/firebasejs/4.12.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.12.1/firebase-messaging.js');

var config = {
    apiKey: "AIzaSyDdQTWHW0R2Do0lwqndgMWhoNJIctEjLdw",
    authDomain: "midterm-project-105062229.firebaseapp.com",
    databaseURL: "https://midterm-project-105062229.firebaseio.com",
    projectId: "midterm-project-105062229",
    storageBucket: "midterm-project-105062229.appspot.com",
    messagingSenderId: "598126716023"
  };
  firebase.initializeApp(config);

  const messaging=firebase.messaging();

  messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    var notificationTitle = 'Background Message Title';
    var notificationOptions = {
      body: 'Background Message body.',
      icon: '/firebase-logo.png'
    };
  
    return self.registration.showNotification(notificationTitle,
      notificationOptions);
  });