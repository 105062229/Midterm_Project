function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnFb = document.getElementById('btnFB');
    var btnTw = document.getElementById('btnTwitter');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function (e) {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function(e){
            window.location = "index.html";
        })
        .catch(function(e) {
            create_alert("error", e.message);
			txtEmail.value="";
			txtPassword.value="";
        }); 
    });

    var provider_google = new firebase.auth.GoogleAuthProvider();
    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider_google).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location="index.html";
        }).catch(function (e) {
            create_alert("error",e.message);
			txtEmail.value="";
			txtPassword.value="";
        });
    });

    var provider_fb = new firebase.auth.FacebookAuthProvider();
    btnFb.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider_fb).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location="index.html";
        }).catch(function (e) {
            create_alert("error",e.message);
        });
    });

    var provider_tw = new firebase.auth.TwitterAuthProvider();
    btnTw.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider_tw).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location="index.html";
        }).catch(function (e) {
            create_alert("error",e.message);
        });
    });


    btnSignUp.addEventListener('click', function () {        
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(function (e){
            create_alert("success",e.message);
            email="";
            password="";
        }) 
        .catch(function(e){
            create_alert("error",e.message);
            email="";
            password="";
        }); 
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};

document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.'); 
      return;
    }
  
    if (Notification.permission !== "granted")
      Notification.requestPermission();
  });
  
  function notifyMe() {
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
      var notification = new Notification("Stanley's Midterm Project", {
        icon: 'https://firebasestorage.googleapis.com/v0/b/midterm-project-105062229.appspot.com/o/images%2Ffirebase.png?alt=media&token=431833f7-da4a-497a-a998-ac26c6dd3f0f',
        body: "New message",
      });
  
      notification.onclick = function () {
        window.open("http://stackoverflow.com/a/13328397/1269037");   
        notification.close();   
      };
  
    }
  
  }