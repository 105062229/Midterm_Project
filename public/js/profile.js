var count=false;
function init() {
    count=false;
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        
        var user_status = document.getElementById('signin');
        var link_f = document.getElementById('food');
        var link_m = document.getElementById('music');
        var link_s = document.getElementById('sport');
        // Check user login
        if (user) {
            console.log(user.uid);
            user_email = user.email;
            user_status.innerHTML = '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
            + user_email +'</a><div id="dynamic-menu" class="dropdown-menu" aria-labelledby="dropdown01"><a class="dropdown-item" id="logout-btn">Log out</a><a class="dropdown-item" id="profile">Profile</a></div></li>'
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function(e) {
                firebase.auth().signOut().then(function(e){
                    alert("success");
                }).catch(function(e){
                    alert("error");
                });
                window.location.replace("index.html");
            });
            var f=document.getElementById("F");
            f.addEventListener('click',function(e){
                if(valid){

                }
                alert("Error !! Please click on submit button\n\nIf no need to change just leave all the blanks empty and click on submit button");
            });
            var m=document.getElementById("M");
            m.addEventListener('click',function(e){
                alert("Error !! Please click on submit button\n\nIf no need to change just leave all the blanks empty and click on submit button");
            });
            var s=document.getElementById("S");
            s.addEventListener('click',function(e){
                alert("Error !! Please click on submit button\n\nIf no need to change just leave all the blanks empty and click on submit button");
            });

            var valid = false;
            var btnProfile = document.getElementById('profile');
            btnProfile.addEventListener('click', function(e){
                if(valid){
                    window.location.replace("profile.html");
                }
                else{
                    alert("Error !! Please click on submit button\n\nIf no need to change just leave all the blanks empty and click on submit button");
                }
            });

            var uploadFileInput = document.getElementById("uploadFileInput");
            var storageRef = firebase.storage().ref();
            var downloadURL;
            uploadFileInput.addEventListener("change", function(){
                var file = this.files[0];
                var uploadTask = storageRef.child('images/'+ user.uid).put(file);
                uploadTask.on('state_changed', function(snapshot){
                    // 觀察狀態變化，例如：progress, pause, and resume

                    // 取得檔案上傳狀態，並用數字顯示
                    var prg = document.getElementById('progress');
                    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    console.log('Upload is ' + progress + '% done');
                    prg.value=progress;
                    if(progress<100){
                        document.getElementById('submit').disabled=true;
                    }
                    else{
                        document.getElementById('submit').disabled=false;
                    }
                    switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED: // or 'paused'

                        console.log('Upload is paused');
                        break;
                    case firebase.storage.TaskState.RUNNING: // or 'running'

                        console.log('Upload is running');
                         break;
                    }
                }, function(error) {
                    // Handle unsuccessful uploads
                    alert(error.message);
                }, function() {
                    // Handle successful uploads on complete

                    // For instance, get the download URL: https://firebasestorage.googleapis.com/...

                    downloadURL = uploadTask.snapshot.downloadURL;
                    
                 });
            },false);

            

            var submit = document.getElementById('submit');
            submit.addEventListener("click",function(){
                var name = $('#name').val();
                if(name.length==0){
                    name=user.displayName;
                    console.log(1231231);
                }
                user.updateProfile({
                    displayName: name,
                    photoURL: downloadURL,
                });
                console.log(user.displayName);
                console.log(user.photoURL);
                alert("Save changes");
                valid = true;
                console.log(valid);
                count=true;
                link_f.innerHTML = '<a class="nav-link" href="food.html">Food</a>';
                link_m.innerHTML = '<a class="nav-link" href="music.html">Music</a>'
                link_s.innerHTML = '<a class="nav-link" href="sport.html">Sport</a>'
                /*firebase.database().ref('food_list').once('value').then(function(snapshot){
                    snapshot.forEach(function(childSnapshot){
                        childSnapshot.ref.update({displayName: user.displayName});
                        console.log(user.displayName);
                    })
                });
                
*/              
                
                
            })
            
            var postsRef_food = firebase.database().ref('food_list');
            var hf_btn = document.getElementById("hf_btn");
            
            

            postsRef_food.once('value')
                .then(function(snapshot){
                    snapshot.forEach(function(childSnapshot){
                        var childData = childSnapshot.val();
                        if(childData.email == user.email){
                            var post = document.getElementById('post_f');
                            if(childData.data!=""){
                                var data = document.createTextNode(String(childData.data));
                                post.appendChild(data);
                                var enter = document.createElement('br');
                                post.appendChild(enter);
                            }
                            else{
                                var DOM_img = document.createElement("img");
                                DOM_img.src = childData.uploadImg;
                                DOM_img.style.width='40%';
                                post.appendChild(DOM_img);
                                var enter = document.createElement('br');
                                post.appendChild(enter);
                            }
                        }
                    })
                });
            
                var postsRef_music = firebase.database().ref('music_list');
                var hm_btn = document.getElementById("hm_btn");
                
                postsRef_music.once('value')
                    .then(function(snapshot){
                        snapshot.forEach(function(childSnapshot){
                            var childData = childSnapshot.val();
                            if(childData.email == user.email){
                                var post = document.getElementById('post_m');
                                if(childData.data!=""){
                                    var data = document.createTextNode(String(childData.data));
                                    post.appendChild(data);
                                    var enter = document.createElement('br');
                                    post.appendChild(enter);
                                }
                                else{
                                    var DOM_img = document.createElement("img");
                                    DOM_img.src = childData.uploadImg;
                                    DOM_img.style.width='40%';
                                    post.appendChild(DOM_img);
                                    var enter = document.createElement('br');
                                    post.appendChild(enter);
                                }
                            }
                        })
                    });

                    var postsRef_sport = firebase.database().ref('sport_list');
                    var hs_btn = document.getElementById("hs_btn");
                    
                    postsRef_sport.once('value')
                        .then(function(snapshot){
                            snapshot.forEach(function(childSnapshot){
                                var childData = childSnapshot.val();
                                if(childData.email == user.email){
                                    var post = document.getElementById('post_s');
                                    if(childData.data!=""){
                                        var data = document.createTextNode(String(childData.data));
                                        post.appendChild(data);
                                        var enter = document.createElement('br');
                                        post.appendChild(enter);
                                    }
                                    else{
                                        var DOM_img = document.createElement("img");
                                        DOM_img.src = childData.uploadImg;
                                        DOM_img.style.width='40%';
                                        post.appendChild(DOM_img);
                                        var enter = document.createElement('br');
                                        post.appendChild(enter);
                                    }
                                }
                            })
                        });

        } else {
            // It won't show any post if not login
            user_status.innerHTML = '<a class="nav-link" href="signin.html">Log in</a>';
            //document.getElementById('post_list').innerHTML = "";
        }
    });


    // Create a storage reference from our storage service



}



window.onload = function () {
    init();
};


function hf(){
    document.getElementById('post_f').style.display = 'block';
    document.getElementById('post_m').style.display = 'none';
    document.getElementById('post_s').style.display = 'none';
}

function hm(){
    document.getElementById('post_f').style.display = 'none';
    document.getElementById('post_m').style.display = 'block';
    document.getElementById('post_s').style.display = 'none';
}

function hs(){
    document.getElementById('post_f').style.display = 'none';
    document.getElementById('post_m').style.display = 'none';
    document.getElementById('post_s').style.display = 'block';
}

function Myalert(){
    if(!count){
        alert("change isn't saved\nRemember to click on submit");
    }
}

document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.'); 
      return;
    }
  
    if (Notification.permission !== "granted")
      Notification.requestPermission();
  });
  
  function notifyMe() {
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
      var notification = new Notification("Stanley's Midterm Project", {
        icon: 'https://firebasestorage.googleapis.com/v0/b/midterm-project-105062229.appspot.com/o/images%2Ffirebase.png?alt=media&token=431833f7-da4a-497a-a998-ac26c6dd3f0f',
        body: "New message",
      });
  
      notification.onclick = function () {
        window.open("http://stackoverflow.com/a/13328397/1269037");   
        notification.close();   
      };
  
    }
  
  }