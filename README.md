# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. user page (profile)
    2. post page (Topic)
    3. post list page (profile personal history)
    4. leave comment under any post (留言)
* Other functions (add/delete)
    1. email verified
    2. set nickname and photo
    3. audio
    4. send image
    5. notification
    6. Animation (snow in the index.html)


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
    authentication by email,google,facebook,twitter
    three topic to choose (so far only the food.html is finished)
    notificated when new post
    RWD in food.html
## Security Report (Optional)
    user needs to verify email to keep on using this app,otherwise all the button is disabled,and the window will 
    keep on jumping out alert
    need to sign in to use forum

## Deploy page URL
    https://105062229.gitlab.io/Midterm_Project
## Hosting URL
    https://midterm-project-105062229.firebaseapp.com